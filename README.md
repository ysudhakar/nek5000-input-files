# README #

Nek5000 (https://github.com/Nek5000) is a highly parallel open source spectral element code used mainly to solve 
incompressible Navier-Stokes equations. This repository contains additional INPUT files to simulate turbulent
flows over various configurations using Nek5000.
